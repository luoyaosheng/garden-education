# garden-education

文档地址：xxx

### 什么是智能养花？

通过硬件加持来完成对植物生长环境数据可视化（或远程操作），从而使植物更好的生长。

在本专栏中，我们将自己动手完成硬件、平台搭建及APP/小程序的开发，将会是经历一个从**草稿**（需求分析、功能规划、原型设计及UI设计）到**研发**（设备端开发、平台端开发及APP/小程序开发）再到**市场**（推广网站制作和发布市场）的**完整产品从0到1的生命线**过程。

### 专栏亮点

- 从0开始搭建智能产品的过程
- 需求分析，从想法到实际，学会绘制原型和UI设计
- 实战代码，硬件、接口、APP/小程序、管理后台及推广网站都不落下
- 发布市场，小程序、iOS和Android一个都不能少

### 经验沉淀，教你快速【搭积木】

- 使用开源软件或组件，快速完成开发工作【本专栏开源地址：[garden-education](https://gitee.com/luoyaosheng/garden-education.git)】
- 创客思想，快速实现0到1

### 你能收货什么？

- 熟悉一个完整产品的全过程
- 学会独立设计一个完整的产品
- 学会独立硬件开发
- 学会独立APP/小程序开发
- 学会独立API开发
- 学会独立管理后台开发
- 学会独立推广网站开发
- 获得一套完整的、可直接使用、代码可运行的全套资源

### 适合人群

- 想做智能硬件创业的创客者（了解才能做好产品）
- 产品经理（了解开发过程，便于更好与开发交流及产品设计）
- APP/小程序开发工程师（了解原理及他端的开发，便于对接接口）
- 前端工程师（了解原理及他端的开发，便于对接接口）
- 项目经理（最该看的人，哈哈~~）
- UI设计师（了解原理及他端的开发，便于设计）
- 硬件工程师（了解原理及他端的开发，便于开发）

### 工具与语言

- 需求规整：[XMind](https://www.xmind.cn/)

- 原型设计：[Mockplus](https://www.mockplus.cn/)

- UI设计：[Sketch](http://www.sketchcn.com/)

- 硬件：ESP8266、RGB灯、喷雾、水泵、面包板、土壤湿度传感器、光照传感器、温湿度传感器、LED灯若干、杜邦线若干

- 开发工具：GoLand、Clion、Arduino、Docker、MySQL、Redis、postman

- ###### 开发框架：[Goframe](https://goframe.org/display/gf)、[Uni-App](https://uniapp.dcloud.io/)、[NodeMCU](http://www.nodemcu.com/index_cn.html)、[Gfast](http://www.g-fast.cn/)

- 协议：MQTT、API

- 开发语言：C++、Go、Vue

### 专栏结构

#### 第一部分：想法到原型

idea千千万，做成的没几个。从想法到落地，最基本的就是“草稿”可视化。

第一部分，介绍如何使用原型工具Mockplus 绘制原型。

#### 第二部分：UI设计

设计是好看的灵魂，通过UI对原型图进行“着装”。

第二部分，介绍如何使用设计工具Sketch绘制UI。

#### 第三部分：构建平台

平台是整个服务核心，哪怕再小的互联网产品也要规划好。

第三部分，介绍怎么搭建平台。

#### 第四部分：接口开发

为APP/小程序提供API，使用postman做演示。

第四部分，实战通过Go语言编写api接口。

#### 第五部分：APP/小程序开发

当今社会，单独的APP前期很难吸引用户，通过跨平台工具一次开发多端运行是小成本的最佳选择。

第五部分，实战通过UNI开发APP/小程序并上架微信小程序。

#### 第六部分：编写硬件

通过ESP8266/32快速完成硬件到平台的传输功能，简单几步即可让我们完成智能设备华丽升级。

第六部分，实战完成硬件组装和开发工作。

#### 第七部分：管理后台

通过管理后台，为后期扩展做好准备。

第七部分，实战完成一个简易后台，对接部分硬件数据。

#### 第八部分：总结

不同的人看不同章节，从第二部分到第七部分，是无序的。

第八部分，讲收获谈未来。







### 第五部分：APP/小程序开发

一、uni 快速入门

二、前端 快速入门

只要讲解功能，基本使用，能让文章作者看懂就行。延伸和扩展内容以后再单独开辟。这本书主要写的是让读者快速了解、知道干什么并能直接使用起来。

  https://www.cnblogs.com/shiyanlou/p/12304522.html

  https://class.imooc.com/sale/newfe

https://www.bilibili.com/video/BV13E411B7PF?from=search&seid=11275120215048593737&spm_id_from=333.337.0.0

三、Vue 快速入门

  https://www.w3cschool.cn/uni_app/uni_app-kdmu370c.html

四、开发应用



### 第六部分：编写硬件

一、硬件设备准备（需要Excel表弄好，弄成图片插入）

| 名称                   | 图片 | 数量 |
| ---------------------- | ---- | ---- |
| NodeMcu(ESP8266/ESP32) |      |      |
| 杜邦线                 |      | 若干 |
|                        |      |      |

二、环境搭建

  为尽量降低入门难度，前期会使用 [Arduino IDE](https://www.arduino.cc/en/software) 进行基础硬件的烧写及少些演示，使用 [Clion IDE](https://www.jetbrains.com/clion/) 进行实战项目开发。开发语言也从C转化到C++ 。

1、Arduino IDE 安装及配置

1.1、Arduino IDE 简要介绍及下载

  [Arduino IDE](https://www.arduino.cc/en/main/software) 是由 Arduino 官方提供的支持 **C 语言**的集成开发环境，主要是针对 Arduino 系列的开发板进行编程。

 通过简单的配置，可以在原本的编程环境里添加上对 ESP8266/ESP32 开发板的支持。对于熟悉 Arduino 函数库和开发流程的用户，基本上没有任何使用上的区别。

进入 [Arduino 官网](https://www.arduino.cc/en/software) https://www.arduino.cc/en/software 可以看到，Arduino IDE 已覆盖了Windows、Linux和Mac系统，选择对应版本下载。

![Arduino 可下载版本](https://tva1.sinaimg.cn/large/e6c9d24ely1gzl4o4jjzsj20pp0dkdhn.jpg)

  Windows版下载解压后，双击可执行文件即可打开IDE（建议将整个文件夹放置其他目录下，避免误删）。

![Windows解压后样式](https://tva1.sinaimg.cn/large/e6c9d24ely1gzl51fgwnfj20is0bqdh3.jpg)

双击打开IDE，可以看到IDE提供了中文支持。

![Arduino初始样式](https://tva1.sinaimg.cn/large/e6c9d24ely1gzn8pjhqbuj20ii0dy0te.jpg)

1.2 添加ESP826/ESP32支持

点击 **文件** ，选择 **首选项** ，找到 **附加开发板管理地址** ，点击右侧方框输入以下内容，使其同时支持 ESP8266 和 ESP32 库搜索。

```
http://arduino.esp8266.com/stable/package_esp8266com_index.json
https://dl.espressif.com/dl/package_esp32_index.json
```

![配置ESP支持](https://tva1.sinaimg.cn/large/e6c9d24ely1gzn8vcx1jdj20m60dhq4v.jpg)

点击 **工具** > **开发板** > **开发板管理器** ，进入 **开发板管理器**，输入 esp 进行查询后就可以看到 esp32 和 esp8266 的开发板，其中 esp32选择 **2.0.0** ，esp8266 选择 **3.0.2** 进行安装。

![Arduino开发板管理器](https://tva1.sinaimg.cn/large/e6c9d24ely1gzl62mojotj20lr0c8jtb.jpg)

安装可能会很慢，甚至会失败。建议换个时间点进行安装，推荐早晨六七点。

安装完成后，重启Arduino IDE软件。在 **工具** > **开发板** 选项中即会看到 ESP8266/ESP32 开发板的选项：

![可选开发板](https://tva1.sinaimg.cn/large/e6c9d24ely1gzn8oey0w0j20ik0dx3zw.jpg)

1.3 小例子测验

2、Clion IDE 安装及配置

二、主控芯片选择

